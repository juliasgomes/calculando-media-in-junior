let notas = document.querySelector(".notas");
const add = document.querySelector(".add");
const calc = document.querySelector(".calc");
let media_display = document.querySelector("#media_display");
let section = document.querySelector(".notes_display");


add.addEventListener("click", (e)=>{
    e.preventDefault();
    display();
});

calc.addEventListener("click", (e)=>{
    e.preventDefault();
    printMedia();
})

/* add.addEventListener("click", display); */
calc.addEventListener("click", printMedia);

var notas_ = [];
function display(){
    let input = notas.value;
    if(input <= 10 && input >= 0){
        notas_.push(parseFloat(input));
        let scope = document.createElement("div");
        scope.innerHTML = `
            <div class="notes_list">
                A nota foi ${input}
            </div>
        `
        section.appendChild(scope);
    } else if (input > 10 || input < 0){
        alert ("Nota inválida: Insira uma nota entre 0 e 10");
    } else if (input == undefined){
        alert("Insira uma nota");
    }
    notas.value = "";
}


function media(){
    var soma = 0, media = 0;
    for (i of notas_){
        soma += i;
    }
    media = soma/notas_.length;
    return media;
}

function printMedia(){
    media_display.innerHTML = `A média é: ${media()}`
}